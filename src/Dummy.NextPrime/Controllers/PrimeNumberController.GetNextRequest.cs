﻿namespace Dummy.NextPrime.Controllers
{
    public partial class PrimeNumberController
    {
        public class GetNextRequest
        {
            public int Number { get; set; }
        }
    }
}
