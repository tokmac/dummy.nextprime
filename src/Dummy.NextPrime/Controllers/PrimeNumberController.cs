﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;
using Dummy.NextPrime.PrimeTool;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Dummy.NextPrime.Controllers
{
    [ApiController]
    [Route("number/{theNumber}/prime")]
    public partial class PrimeNumberController : ControllerBase
    {
        private readonly ILogger<PrimeNumberController> _logger;

        public PrimeNumberController(ILogger<PrimeNumberController> logger)
        {
            _logger = logger;
        }


        /// <summary>
        /// Get the same or bigger number of a number which is the next prime one
        /// </summary>
        /// <param name="theNumber">Pivot number</param>
        /// <returns>THE NEXT PRIME</returns>
        [HttpGet]
        [Route("next")]
        public IActionResult GetNext([FromRoute] int theNumber)
        {
            if (PrimeNumberTool.IsPrime(theNumber))
                return Ok(theNumber);

            _logger.LogInformation($"START searching for the next prime number bigger than '{theNumber}'");

            var result = PrimeNumberTool.NextPrime(theNumber);

            _logger.LogInformation($"FOUND the next prime number bigger than '{theNumber}', and the result is '{result}'");

            return Ok(result);
        }
    }
}
