using Dummy.NextPrime.PrimeTool;
using System;
using Xunit;

namespace Dummy.NextPrime.PrimeTool.Tests
{
    public class PrimeTool_NextPrimeShould
    {
        [Fact]
        public void NextPrime_InputIs1_ReturnTrue()
        {
            var result = PrimeNumberTool.NextPrime(1);

            Assert.True(result == 2, "from 1 the next prime should be 2");
        }

        [Fact]
        public void NextPrime_InputIsMinus2000_ReturnTrue()
        {
            var result = PrimeNumberTool.NextPrime(-2000);

            Assert.True(result == 2, "from -2000 the next prime should be 2");
        }

        [Fact]
        public void NextPrime_InputIsMinus2000_ReturnFalse()
        {
            var result = PrimeNumberTool.NextPrime(-2000);

            Assert.False(result == 1, "from -2000 the next prime should not be 1");
        }

        [Fact]
        public void NextPrime_InputIs10000_ReturnTrue()
        {
            var result = PrimeNumberTool.NextPrime(10000);

            Assert.True(result == 10007, "from 1000 the next prime should not be 10 007");
        }
    }
}
