using Dummy.NextPrime.PrimeTool;
using System;
using Xunit;

namespace Dummy.NextPrime.PrimeTool.Tests
{
    public class PrimeTool_IsPrimeShould
    {
        [Fact]
        public void IsPrime_InputIs2_ReturnTrue()
        {
            var result = PrimeNumberTool.IsPrime(2);

            Assert.True(result, "2 should be prime");
        }

        [Fact]
        public void IsPrime_InputIs9689_ReturnTrue()
        {
            var result = PrimeNumberTool.IsPrime(9689);

            Assert.True(result, "9689 should be prime");
        }

        [Fact]
        public void IsPrime_InputIs1_ReturnFalse()
        {
            var result = PrimeNumberTool.IsPrime(1);

            Assert.False(result, "1 should not be prime");
        }

        [Fact]
        public void IsPrime_InputIs9688_ReturnFalse()
        {
            var result = PrimeNumberTool.IsPrime(9688);

            Assert.False(result, "9688 should not be prime");
        }
    }
}
