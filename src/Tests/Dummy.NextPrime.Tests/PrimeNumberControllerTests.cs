using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Dummy.NextPrime.Tests
{
    public class PrimeNumberControllerTests
    : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> _factory;

        public PrimeNumberControllerTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task Get_NumberPrimeNextEndpointReturnSuccess()
        {
            var client = _factory.CreateClient();
            var testNumber = new Random().Next(int.MinValue, int.MaxValue);

            var response = await client.GetAsync($"/number/{testNumber}/prime/next");

            var exception = Record.Exception(() => response.EnsureSuccessStatusCode());
            Assert.Null(exception);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(19)]
        [InlineData(167)]
        [InlineData(433)]
        [InlineData(523)]
        [InlineData(541)]
        public async Task Get_NumberPrimeNextEndpointReturnCorrectResponse_InputPrimeNumbers(int number)
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync($"/number/{number}/prime/next");
            var result = await response.Content.ReadAsStringAsync();

            Assert.Equal(number, int.Parse(result));
        }

        [Theory]
        [InlineData(8, 11)]
        [InlineData(1036, 1039)]
        [InlineData(9420, 9421)]
        public async Task Get_NumberPrimeNextEndpointReturnCorrectResponse_InputNotPrimeNumbers(int number, int expectedResult)
        {
            var client = _factory.CreateClient();

            var response = await client.GetAsync($"/number/{number}/prime/next");
            var result = await response.Content.ReadAsStringAsync();

            Assert.Equal(expectedResult, int.Parse(result));
        }
    }
}
