# Dummy.NextPrime

This is the dummiest way to find a prime number through hosting your own tiny API and using it.

### What is this repository for?

- This is a dummy project which does as little as finding the next prime number which is bigger or equal to the given number.
- The repo consists of:

  - Tiny REST API
  - Helper Project for working with prime numbers
  - API Integration Test project
  - Unit Tests for the Helper Project

- The REST API has a configured Logger (Serilog) which currently is sinking only to the Console.

- In the repo, you can also find a "dockerfile" which builds a docker image that can be easily deployed as a container.

Nothing more...nothing less...

### How do I get set up?

If you just want to run API you need to do the following steps:
- Execute the following command into the root folder of this repo:

```docker build -t dummy-prime-api:latest``` 

Then execute the next command which would run a new container with the newly built image:

```docker run --rm -it -p 8080:80 dummy-prime-api:latest```

Voila! You are up and running...

### Usage

![](https://static.seibertron.com/images/misc/uploads/1485189514-power-of-the-primes-voting.jpg)

If everything went smoothly until now ( "60% of the time, it works every time" ), you are ready to find THE NEXT PRIME!

In order to do this you would need to hit the following URL, where you would need to replace '{number}' with the number you want to try.

```http://localhost:8080/number/{number}/prime/next```

You can also check on the following ling for a Swagger UI where you can also experiment.

```http://localhost:8080```

CHEERS! 
