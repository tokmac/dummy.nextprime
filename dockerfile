FROM mcr.microsoft.com/dotnet/core/sdk:3.1-alpine AS build
WORKDIR /app
# copy sln and csproj files into the image
COPY src/*.sln .
COPY src/Dummy.NextPrime/*.csproj ./Dummy.NextPrime/
COPY src/Dummy.NextPrime.PrimeTool/*.csproj ./Dummy.NextPrime.PrimeTool/
COPY src/Tests/Dummy.NextPrime.PrimeTool.Tests/*.csproj ./Tests/Dummy.NextPrime.PrimeTool.Tests/
COPY src/Tests/Dummy.NextPrime.Tests/*.csproj ./Tests/Dummy.NextPrime.Tests/
# restore package dependencies for the solution
RUN dotnet restore
# copy full solution over
COPY src/. .
# build the solution
RUN dotnet build



# create a new build target called testrunner
FROM build AS primetooltestrunner
# navigate to the unit test directory
WORKDIR /app/Tests/Dummy.NextPrime.PrimeTool.Tests
# when you run this build target it will run the unit tests
CMD ["dotnet", "test", "--logger:trx"]

# run the unit tests
FROM build AS primetooltest
# set the directory to be within the unit test project
WORKDIR /app/Tests/Dummy.NextPrime.PrimeTool.Tests
# run the unit tests
RUN dotnet test --logger:trx



# create a new build target called testrunner
FROM build AS apitestrunner
# navigate to the unit test directory
WORKDIR /app/Tests/Dummy.NextPrime.Tests
# when you run this build target it will run the unit tests
CMD ["dotnet", "test", "--logger:trx"]

# run the unit tests
FROM build AS apitest
# set the directory to be within the unit test project
WORKDIR /app/Tests/Dummy.NextPrime.Tests
# run the unit tests
RUN dotnet test --logger:trx



# create a new layer from the build later
FROM build AS publish
# set the working directory to be the web api project
WORKDIR /app/Dummy.NextPrime/
# publish the web api project to a directory called out
RUN dotnet publish -c Release -o out

# create a new layer using the cut-down aspnet runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-alpine AS runtime
WORKDIR /app
# copy over the files produced when publishing the service
COPY --from=publish /app/Dummy.NextPrime/out ./
# expose port 80 as our application will be listening on this port
EXPOSE 80
# run the web api when the docker image is started
ENTRYPOINT ["dotnet", "Dummy.NextPrime.dll"]